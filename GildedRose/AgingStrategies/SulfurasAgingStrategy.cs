using GildedRose.Interfaces;

namespace GildedRose.AgingStrategies
{
    public class SulfurasAgingStrategy : IItemAgingStrategy
    {
        public void ApplyAging(Item item)
        {
            //It shouldn't change the item at all
        }
    }
}