using GildedRose.Interfaces;

namespace GildedRose.AgingStrategies
{
    public class ConjuredItemAgingStrategy : IItemAgingStrategy
    {
        public void ApplyAging(Item item)
        {
            var qualityPenalty = item.SellIn <= 0 ? 4 : 2;

            item.Quality = Utils.Clamp(item.Quality - qualityPenalty, 0, 50);

            item.SellIn = item.SellIn - 1;
        }
    }
}