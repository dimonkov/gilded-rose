using GildedRose.Interfaces;

namespace GildedRose.AgingStrategies
{
    public class BackstagePassesAgingStrategy : IItemAgingStrategy
    {
        public void ApplyAging(Item item)
        {
            if(item.SellIn <= 0)
                item.Quality = 0;
            else if(item.SellIn <= 5)
                ApplyAging5Before(item);
            else if(item.SellIn <= 10)
                ApplyAging10Before(item);
            else
                ApplyNormalAging(item);

            item.Quality = Utils.Clamp(item.Quality, 0, 50);

            item.SellIn = item.SellIn - 1;
        }

        protected void ApplyNormalAging(Item item)
        {
            item.Quality = item.Quality + 1;
        }

        protected void ApplyAging10Before(Item item)
        {
            item.Quality = item.Quality + 2;
        }

        protected void ApplyAging5Before(Item item)
        {
            item.Quality = item.Quality + 3; 
        }
    }
}