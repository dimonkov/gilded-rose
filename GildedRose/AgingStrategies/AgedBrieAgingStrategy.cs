using GildedRose.Interfaces;

namespace GildedRose.AgingStrategies
{
    public class AgedBrieAgingStrategy : IItemAgingStrategy
    {
        public void ApplyAging(Item item)
        {
            //Quality improves faster after sellin
            var qualityIncrease = item.SellIn > 0 ? 1 : 2;
            
            item.Quality = Utils.Clamp(item.Quality + qualityIncrease, 0, 50);

            item.SellIn = item.SellIn - 1;
        }
    }
}