﻿using GildedRose.Interfaces;
using System;

namespace GildedRose.Rules
{
    /// <summary>
    /// Stores a value to be used if the condition is met.
    /// </summary>
    /// <typeparam name="TCondition">Condition input type</typeparam>
    /// <typeparam name="T">Attached value type</typeparam>
    public class Rule<TCondition, T> : IRule<TCondition, T>
    {
        /// <summary>
        /// Rule that indicated whether the consumer should use the attached value
        /// or find another candidate.
        /// </summary>
        public Func<TCondition, bool> RuleSatisfied { get; protected set; }

        /// <summary>
        /// Value attached to this rule
        /// </summary>
        public T Item { get; protected set; }

        /// <summary>
        /// Creates a new instance of a rule.
        /// </summary>
        /// <param name="condition">Condition for this rule to be true</param>
        /// <param name="item">Attached item</param>
        public Rule(Func<TCondition, bool> condition, T item)
        {
            RuleSatisfied = condition;

            Item = item;
        }
    }
}
