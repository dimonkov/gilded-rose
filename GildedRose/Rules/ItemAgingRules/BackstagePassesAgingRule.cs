﻿using GildedRose.AgingStrategies;
using GildedRose.Interfaces;

namespace GildedRose.Rules.ItemAgingRules
{
    public class BackstagePassesAgingRule : Rule<Item, IItemAgingStrategy>
    {
        public BackstagePassesAgingRule() : base(item => item.Name.StartsWith("Backstage passes"),
                new BackstagePassesAgingStrategy())
        { }
    }
}
