﻿using GildedRose.AgingStrategies;
using GildedRose.Interfaces;

namespace GildedRose.Rules.ItemAgingRules
{
    public class SulfurasAgingRule : Rule<Item, IItemAgingStrategy>
    {
        public SulfurasAgingRule() : base(item => item.Name.StartsWith("Sulfuras"),
                new SulfurasAgingStrategy()) { }
    }
}
