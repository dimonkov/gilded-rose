﻿using GildedRose.AgingStrategies;
using GildedRose.Interfaces;

namespace GildedRose.Rules.ItemAgingRules
{
    public class NormalItemAgingRule : Rule<Item, IItemAgingStrategy>
    {
        public NormalItemAgingRule() : base(item => true,
                new NormalItemAgingStrategy())
        { }
    }
}
