﻿using GildedRose.AgingStrategies;
using GildedRose.Interfaces;

namespace GildedRose.Rules.ItemAgingRules
{
    public class AgedBrieAgingRule : Rule<Item, IItemAgingStrategy>
    {
        public AgedBrieAgingRule() : base(item => item.Name.StartsWith("Aged Brie"),
                new AgedBrieAgingStrategy())
        { }
    }
}
