﻿using GildedRose.AgingStrategies;
using GildedRose.Interfaces;

namespace GildedRose.Rules.ItemAgingRules
{
    public class ConjuredItemAgingRule : Rule<Item, IItemAgingStrategy>
    {
        public ConjuredItemAgingRule() : base(item => item.Name.StartsWith("Conjured"),
                new ConjuredItemAgingStrategy())
        { }
    }
}
