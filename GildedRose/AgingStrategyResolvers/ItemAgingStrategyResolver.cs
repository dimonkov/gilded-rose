﻿using GildedRose.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace GildedRose.ItemAgingStrategyResolvers
{
    public class ItemAgingStrategyResolver : IItemAgingStrategyResolver
    {
        public ItemAgingStrategyResolver(IList<IRule<Item, IItemAgingStrategy>> itemAgingRules)
        {
            ItemAgingRules = itemAgingRules;
        }

        public IList<IRule<Item, IItemAgingStrategy>> ItemAgingRules { get; }

        public IItemAgingStrategy GetItemAgingStrategy(Item item)
        {
            return ItemAgingRules.FirstOrDefault(rule => rule.RuleSatisfied(item)).Item;
        }
    }
}
