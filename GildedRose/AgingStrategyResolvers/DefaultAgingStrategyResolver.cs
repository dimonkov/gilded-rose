﻿using GildedRose.Interfaces;
using GildedRose.Rules.ItemAgingRules;
using System.Collections.Generic;

namespace GildedRose.ItemAgingStrategyResolvers
{
    /// <summary>
    /// This class exists purely for convenience of testing.
    /// Shouldn't be used otside.
    /// </summary>
    internal class DefaultItemAgingStrategyResolver : ItemAgingStrategyResolver
    {
        /// <summary>
        /// Used only for testing purposes, for production please use the <see cref="ItemAgingStrategyResolver"/>
        /// </summary>
        /// <param name="includeConjured"></param>
        public DefaultItemAgingStrategyResolver(bool includeConjured) : base(new List<IRule<Item, IItemAgingStrategy>>
        {
            new SulfurasAgingRule(),
            new AgedBrieAgingRule(),
            new BackstagePassesAgingRule(),
        })
        {
            if(includeConjured)
            {
                this.ItemAgingRules.Add(new ConjuredItemAgingRule());
            }

            //TODO: Consider what to do with default rule,
            //Maybe initialize it always in the base class?
            this.ItemAgingRules.Add(new NormalItemAgingRule());
        }
    }
}
