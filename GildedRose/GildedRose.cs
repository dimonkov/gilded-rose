﻿using System;
using System.Collections.Generic;
using GildedRose.Interfaces;

namespace GildedRose
{
    public class GildedRose
    {
        IList<Item> Items;

        protected IItemAgingStrategyResolver ItemAgingStrategyResolver { get; }
        protected IDictionary<Type, IItemAgingStrategy> AgingStrategies { get; }
        public GildedRose(IList<Item> Items, IItemAgingStrategyResolver itemAgingStrategyResolver)
        {
            this.Items = Items;
            ItemAgingStrategyResolver = itemAgingStrategyResolver;
        }

        public IItemAgingStrategy GetItemAgingStrategy(Item item)
            => ItemAgingStrategyResolver.GetItemAgingStrategy(item);

        public void UpdateQuality()
        {
            foreach (var item in Items)
            {
                GetItemAgingStrategy(item).ApplyAging(item);
            }
        }
    }
}
