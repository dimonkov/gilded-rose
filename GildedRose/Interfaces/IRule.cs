﻿using System;

namespace GildedRose.Interfaces
{
    /// <summary>
    /// Stores a value to be used if the condition is met.
    /// </summary>
    /// <typeparam name="TCondition">Condition input type</typeparam>
    /// <typeparam name="T">Attached value type</typeparam>
    public interface IRule<TCondition, T>
    {
        /// <summary>
        /// Rule that indicated whether the consumer should use the attached value
        /// or find another candidate.
        /// </summary>
        Func<TCondition, bool> RuleSatisfied { get; }

        /// <summary>
        /// Value attached to this rule
        /// </summary>
        T Item { get; }
    }
}
