﻿namespace GildedRose.Interfaces
{
    /// <summary>
    /// A service, that given the <see cref="Item"/> will determine
    /// the best aging strategy <see cref="IItemAgingStrategy"/>.
    /// </summary>
    public interface IItemAgingStrategyResolver
    {
        /// <summary>
        /// Get <see cref="IItemAgingStrategy"/> for a particular item
        /// </summary>
        /// <param name="item">Item that needs the aging strategy</param>
        /// <returns>Item aging strategy</returns>
        IItemAgingStrategy GetItemAgingStrategy(Item item);
    }
}
