namespace GildedRose.Interfaces
{
    /// <summary>
    /// A strategy that alters the stats for
    /// a particular item.
    /// </summary>
    public interface IItemAgingStrategy
    {
        /// <summary>
        /// Apply aging cycle to the item (usually after a day passes)
        /// </summary>
        /// <param name="item">Item to work on</param>
        void ApplyAging(Item item);
    }
}