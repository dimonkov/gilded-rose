﻿using Xunit;
using System;
using System.IO;
using System.Text;
using System.Linq;

namespace GildedRose.tests
{
    public class ApprovalTest
    {
        [Fact]
        public void ThirtyDays()
        {
            var lines = File.ReadAllLines("ThirtyDays.txt")
                .Where(l => !string.IsNullOrWhiteSpace(l))
                .ToArray();

            StringBuilder fakeoutput = new StringBuilder();
            Console.SetOut(new StringWriter(fakeoutput));
            Console.SetIn(new StringReader("a\n"));

            Program.Main(new string[] { "-no-conjured" });
            String output = fakeoutput.ToString();

            //For macOS line endings are different
            var outputLines = output.Split('\n', '\r')
                .Where(l => !string.IsNullOrWhiteSpace(l))
                .ToArray();
            for(var i = 0; i<Math.Min(lines.Length, outputLines.Length); i++) 
            {
                Assert.Equal(lines[i], outputLines[i]);
            }
        }
    }
}
