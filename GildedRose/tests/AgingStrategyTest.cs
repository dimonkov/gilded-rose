using Xunit;
using System.Collections.Generic;
using GildedRose.Interfaces;
using System;
using GildedRose.AgingStrategies;
using static GildedRose.tests.TestItemNameData;

namespace GildedRose.tests
{
    public class AgingStrategyTest
    {
        #region Normal Item

        [Fact]
        public void GildedRose_NormalItem_Should_Age()
        {
            var sellin = 3;
            var quality = 17;
            IItemAgingStrategy agingStrategy = new NormalItemAgingStrategy();

            var item = new Item { Name = NormalItemName, SellIn = sellin, Quality = quality };
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(sellin - 1, item.SellIn);
            Assert.Equal(quality - 1, item.Quality);
        }

        [Fact]
        public void GildedRose_NormalItem_Should_Age_2x_Faster_After_SellIn_Expires()
        {
            var sellin = 0;
            var quality = 17;
            IItemAgingStrategy agingStrategy = new NormalItemAgingStrategy();
            var item = new Item { Name = NormalItemName, SellIn = sellin, Quality = quality } ;
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(quality - 2, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality - 4, item.Quality);
        }

        #endregion

        #region Aged Brie
        [Fact]
        public void GildedRose_AgedBrie_Should_Increase_Quality()
        {
            var sellin = 10;
            var quality = 17;
            IItemAgingStrategy agingStrategy = new AgedBrieAgingStrategy();
            var item = new Item { Name = AgedBrieItemName, SellIn = sellin, Quality = quality };
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(quality + 1, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality + 2, item.Quality);
        }

        [Fact]
        public void GildedRose_AgedBrie_Should_Increase_Quality_2x_After_SellIn_Expires()
        {
            var sellin = 0;
            var quality = 17;
            IItemAgingStrategy agingStrategy = new AgedBrieAgingStrategy();
            var item = new Item { Name = AgedBrieItemName, SellIn = sellin, Quality = quality };
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(quality + 2, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality + 4, item.Quality);
        }

        #endregion

        #region Limits

        [Theory]
        [MemberData(nameof(AlwaysPositiveQualityTestData))]
        public void GildedRose_Item_Quality_Should_Always_Be_Positive(Tuple<IItemAgingStrategy, Item> itemData)
        {
            IItemAgingStrategy agingStrategy = itemData.Item1;
            var item = itemData.Item2;
            
            agingStrategy.ApplyAging(item);

            Assert.True(item.Quality >= 0);
        }

        public static IEnumerable<object[]> AlwaysPositiveQualityTestData()
        {
            return new List<object[]>
            {
                new object[]{new Tuple<IItemAgingStrategy, Item>(new NormalItemAgingStrategy(), new Item{Name = NormalItemName, Quality = 1, SellIn = -1}) },
                new object[]{new Tuple<IItemAgingStrategy, Item>(new NormalItemAgingStrategy(), new Item{Name = NormalItemName, Quality = 0, SellIn = -1}) },
                new object[]{ new Tuple<IItemAgingStrategy, Item>(new AgedBrieAgingStrategy(), new Item{Name = AgedBrieItemName, Quality = 1, SellIn = -1}) },
                new object[]{ new Tuple<IItemAgingStrategy, Item>(new SulfurasAgingStrategy(), new Item{Name = SulfurasItemName, Quality = 80, SellIn = -1}) },
                new object[]{ new Tuple<IItemAgingStrategy, Item>(new BackstagePassesAgingStrategy(), new Item{Name = BackstagePassesItemName, Quality = 17, SellIn = 1}) },
                new object[]{ new Tuple<IItemAgingStrategy, Item>(new BackstagePassesAgingStrategy(), new Item{Name = BackstagePassesItemName, Quality = 17, SellIn = 3}) },
                new object[]{ new Tuple<IItemAgingStrategy, Item>(new BackstagePassesAgingStrategy(), new Item{Name = BackstagePassesItemName, Quality = 17, SellIn = 7}) },
                new object[]{ new Tuple<IItemAgingStrategy, Item>(new BackstagePassesAgingStrategy(), new Item{Name = BackstagePassesItemName, Quality = 17, SellIn = 12}) },
                new object[]{ new Tuple<IItemAgingStrategy, Item>(new ConjuredItemAgingStrategy(), new Item{Name = ConjuredItemName, Quality = 1, SellIn = 12}) },
                new object[]{ new Tuple<IItemAgingStrategy, Item>(new ConjuredItemAgingStrategy(), new Item{Name = ConjuredItemName, Quality = 1, SellIn = 0}) },
            };
        }

        [Theory]
        [MemberData(nameof(QualityMaxTestData))]
        public void GildedRose_AgingItems_Quality_Should_Always_Be_Less_Or_Equal_To_50(Tuple<IItemAgingStrategy,Item> itemData)
        {
            IItemAgingStrategy agingStrategy = itemData.Item1;
            var item = itemData.Item2;

            //Simulate aging process
            agingStrategy.ApplyAging(item);

            Assert.True(item.Quality <= 50);
        }

        public static IEnumerable<object[]> QualityMaxTestData()
        {
            return new List<object[]>
            {
                new object[]{new Tuple<IItemAgingStrategy, Item>(new NormalItemAgingStrategy(),  new Item{Name = NormalItemName, Quality = 1, SellIn = -1})},
                new object[]{new Tuple<IItemAgingStrategy, Item>(new AgedBrieAgingStrategy(),  new Item{Name = AgedBrieItemName, Quality = 49, SellIn = -1})},
                new object[]{new Tuple<IItemAgingStrategy, Item>(new BackstagePassesAgingStrategy(),  new Item{Name = BackstagePassesItemName, Quality = 49, SellIn = 12})},
                new object[]{new Tuple<IItemAgingStrategy, Item>(new BackstagePassesAgingStrategy(),  new Item{Name = BackstagePassesItemName, Quality = 49, SellIn = 7})},
                new object[]{new Tuple<IItemAgingStrategy, Item>(new BackstagePassesAgingStrategy(),  new Item{Name = BackstagePassesItemName, Quality = 49, SellIn = 3})},
                new object[]{new Tuple<IItemAgingStrategy, Item>(new BackstagePassesAgingStrategy(),  new Item{Name = BackstagePassesItemName, Quality = 49, SellIn = 1})},
            };
        }

        #endregion

        #region Sulfuras
        [Fact]
        public void GildedRose_Sulfuras_Never_Changes()
        {
            IItemAgingStrategy agingStrategy = new SulfurasAgingStrategy();
            var item = new Item{Name = SulfurasItemName, Quality = 80, SellIn = -1};
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(80, item.Quality);
            Assert.Equal(-1, item.SellIn);
        }
        #endregion

        #region Backstage passes
        [Fact]
        public void GildedRose_Backstage_Passes_Quality_Increases_By_1_If_More_Than_10_Days_Till_Concert()
        {
            var sellin = 15;
            var quality = 10;
            IItemAgingStrategy agingStrategy = new BackstagePassesAgingStrategy();
            var item = new Item { Name = BackstagePassesItemName, SellIn = sellin, Quality = quality };
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(quality + 1, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality + 2, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality + 3, item.Quality);
        }

        [Fact]
        public void GildedRose_Backstage_Passes_Quality_Increases_By_2_If_Less_Or_Equal_10_Days_Till_Concert()
        {
            var sellin = 10;
            var quality = 10;
            IItemAgingStrategy agingStrategy = new BackstagePassesAgingStrategy();
            var item = new Item { Name = BackstagePassesItemName, SellIn = sellin, Quality = quality };
            
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(quality + 2, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality + 4, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality + 6, item.Quality);
        }

        [Fact]
        public void GildedRose_Backstage_Passes_Quality_Increases_By_3_If_Less_Or_Equal_5_Days_Till_Concert()
        {
            var sellin = 5;
            var quality = 10;
            IItemAgingStrategy agingStrategy = new BackstagePassesAgingStrategy();
            var item = new Item { Name = BackstagePassesItemName, SellIn = sellin, Quality = quality };
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(quality + 3, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality + 6, item.Quality);

            agingStrategy.ApplyAging(item);
            Assert.Equal(quality + 9, item.Quality);
        }

        [Fact]
        public void GildedRose_Backstage_Passes_Quality_Drops_To_0_After_Concert()
        {
            var sellin = 0;
            var quality = 10;
            IItemAgingStrategy agingStrategy = new BackstagePassesAgingStrategy();

            var item = new Item { Name = BackstagePassesItemName, SellIn = sellin, Quality = quality };
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(0, item.Quality);
        }

        #endregion

        #region Conjured Items

        [Fact]
        public void GildedRose_ConjuredItem_Should_Degrade_Twice_Faster()
        {
            var sellin = 1;
            var quality = 17;
            IItemAgingStrategy agingStrategy = new ConjuredItemAgingStrategy();

            var item = new Item { Name = ConjuredItemName, SellIn = sellin, Quality = quality };
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(sellin - 1, item.SellIn);
            Assert.Equal(quality - 2, item.Quality);
        }

        [Fact]
        public void GildedRose_ConjuredItem_Should_Degrade_4x_Faster_After_Sellin()
        {
            var sellin = 0;
            var quality = 17;
            IItemAgingStrategy agingStrategy = new ConjuredItemAgingStrategy();

            var item = new Item { Name = ConjuredItemName, SellIn = sellin, Quality = quality };
            
            agingStrategy.ApplyAging(item);

            Assert.Equal(sellin - 1, item.SellIn);
            Assert.Equal(quality - 4, item.Quality);
        }
        #endregion
    }
}