﻿namespace GildedRose.tests
{
    public static class TestItemNameData
    {
        public static string NormalItemName => "Elixir of the Mongoose";

        public static string AgedBrieItemName => "Aged Brie";

        public static string SulfurasItemName => "Sulfuras, Hand of Ragnaros";

        public static string BackstagePassesItemName => "Backstage passes to a TAFKAL80ETC concert";

        public static string ConjuredItemName => "Conjured Mana Cake";
    }
}
