﻿using GildedRose.Rules.ItemAgingRules;
using System.Collections.Generic;
using Xunit;
using static GildedRose.tests.TestItemNameData;

namespace GildedRose.tests
{
    public static class RuleTests
    {
        #region Normal Item
        [Theory]
        [MemberData(nameof(AllSupportedObjects))]
        public static void Rule_Test_Normal_Item_Rule_Matches_Any(Item item)
        {
            var itemRule = new NormalItemAgingRule();

            var matches = itemRule.RuleSatisfied(item);

            Assert.True(matches);
        }

        public static IEnumerable<object[]> AllSupportedObjects()
        {
            return new List<object[]>
            {
                new object[]{ new Item{Name = "test", Quality = 1, SellIn = -1} },
                new object[]{ new Item{Name = "supertest", Quality = 1, SellIn = -1} },
                new object[]{ new Item{Name = "apple", Quality = 80, SellIn = -1} },
                new object[]{ new Item{Name = "orange", Quality = 17, SellIn = 1} },
                new object[]{ new Item{Name = "foo", Quality = 17, SellIn = 12} }
            };
        }

        #endregion

        #region Aged Brie
        [Fact]
        public static void Rule_Test_AgedBrie_Rule_Matches_Item()
        {
            var item = new Item { Name = AgedBrieItemName, Quality = 6, SellIn = 1 };

            var itemRule = new AgedBrieAgingRule();

            var matches = itemRule.RuleSatisfied(item);

            Assert.True(matches);
        }

        #endregion

        #region Sulfuras
        [Fact]
        public static void Rule_Test_Sulfuras_Rule_Matches_Item()
        {
            var item = new Item { Name = SulfurasItemName, Quality = 6, SellIn = 1 };

            var itemRule = new SulfurasAgingRule();

            var matches = itemRule.RuleSatisfied(item);

            Assert.True(matches);
        }

        #endregion

        #region BackStage passes
        [Fact]
        public static void Rule_Test_BackstagePasses_Rule_Matches_Item()
        {
            var item = new Item { Name = BackstagePassesItemName, Quality = 6, SellIn = 1 };

            var itemRule = new BackstagePassesAgingRule();

            var matches = itemRule.RuleSatisfied(item);

            Assert.True(matches);
        }

        #endregion

        #region Conjured
        [Fact]
        public static void Rule_Test_Conjured_Rule_Matches_Item()
        {
            var item = new Item { Name = ConjuredItemName, Quality = 6, SellIn = 1 };

            var itemRule = new ConjuredItemAgingRule();

            var matches = itemRule.RuleSatisfied(item);

            Assert.True(matches);
        }

        #endregion
    }
}
