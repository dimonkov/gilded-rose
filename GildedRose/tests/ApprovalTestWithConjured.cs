using Xunit;
using System;
using System.IO;
using System.Text;
using System.Linq;

namespace GildedRose.tests
{
    public class ApprovalTestWithConjured
    {
        [Fact]
        public void ApprovalTestWithConjured_ThirtyDays()
        {
            var lines = File.ReadAllLines("ThirtyDaysWithConjuredSupport.txt")
                .Where(l => !string.IsNullOrWhiteSpace(l))
                .ToArray();

            StringBuilder fakeoutput = new StringBuilder();
            Console.SetOut(new StringWriter(fakeoutput));
            Console.SetIn(new StringReader("a\n"));

            Program.Main(new string[] { });
            String output = fakeoutput.ToString();

            //For macOS line endings are different
            var outputLines = output.Split('\n', '\r')
                .Where(l => !string.IsNullOrWhiteSpace(l))
                .ToArray();

            for(var i = 0; i<Math.Min(lines.Length, outputLines.Length); i++) 
            {
                Assert.Equal(lines[i], outputLines[i]);
            }
        }
    }
}
