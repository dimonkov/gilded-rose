﻿using Xunit;
using System.Collections.Generic;
using GildedRose.Interfaces;
using GildedRose.ItemAgingStrategyResolvers;
using static GildedRose.tests.TestItemNameData;

namespace GildedRose.tests
{
    public class GildedRoseTest
    {
        protected IItemAgingStrategyResolver ItemAgingStrategyResolver = new DefaultItemAgingStrategyResolver(true);


        const string ConjuredItemName = "Conjured Mana Cake";

        #region Normal Item
        [Fact]
        public void GildedRose_NormalItem_Should_Age()
        {
            var sellin = 3;
            var quality = 17;
            IList<Item> Items = new List<Item> { new Item { Name = NormalItemName, SellIn = sellin, Quality = quality } };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            app.UpdateQuality();


            var item = Items[0];
            Assert.Equal(sellin - 1, item.SellIn);
            Assert.Equal(quality - 1, item.Quality);
        }

        [Fact]
        public void GildedRose_NormalItem_Should_Age_2x_Faster_After_SellIn_Expires()
        {
            var sellin = 0;
            var quality = 17;
            IList<Item> Items = new List<Item> { new Item { Name = NormalItemName, SellIn = sellin, Quality = quality } };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            app.UpdateQuality();

            var item = Items[0];
            Assert.Equal(quality - 2, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality - 4, item.Quality);
        }
        #endregion

        #region Aged Brie
        [Fact]
        public void GildedRose_AgedBrie_Should_Increase_Quality()
        {
            var sellin = 10;
            var quality = 17;
            IList<Item> Items = new List<Item> { new Item { Name = AgedBrieItemName, SellIn = sellin, Quality = quality } };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            app.UpdateQuality();

            var item = Items[0];
            Assert.Equal(quality + 1, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality + 2, item.Quality);
        }

        [Fact]
        public void GildedRose_AgedBrie_Should_Increase_Quality_2x_After_SellIn_Expires()
        {
            var sellin = 0;
            var quality = 17;
            IList<Item> Items = new List<Item> { new Item { Name = AgedBrieItemName, SellIn = sellin, Quality = quality } };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            app.UpdateQuality();

            var item = Items[0];
            Assert.Equal(quality + 2, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality + 4, item.Quality);
        }
        #endregion

        #region Limits
        [Theory]
        [MemberData(nameof(AlwaysPositiveQualityTestData))]
        public void GildedRose_Item_Quality_Should_Always_Be_Positive(Item item)
        {
            IList<Item> Items = new List<Item> { item };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            //Simulate aging process
            app.UpdateQuality();

            Assert.True(item.Quality >= 0);
        }

        public static IEnumerable<object[]> AlwaysPositiveQualityTestData()
        {
            return new List<object[]>
            {
                new object[]{ new Item{Name = NormalItemName, Quality = 1, SellIn = -1} },
                new object[]{ new Item{Name = AgedBrieItemName, Quality = 1, SellIn = -1} },
                new object[]{ new Item{Name = SulfurasItemName, Quality = 80, SellIn = -1} },
                new object[]{ new Item{Name = BackstagePassesItemName, Quality = 17, SellIn = 1} },
                new object[]{ new Item{Name = BackstagePassesItemName, Quality = 17, SellIn = 3} },
                new object[]{ new Item{Name = BackstagePassesItemName, Quality = 17, SellIn = 7} },
                new object[]{ new Item{Name = BackstagePassesItemName, Quality = 17, SellIn = 12} }
            };
        }

        [Theory]
        [MemberData(nameof(QualityMaxTestData))]
        public void GildedRose_AgingItems_Quality_Should_Always_Be_Less_Or_Equal_To_50(Item item)
        {
            IList<Item> Items = new List<Item> { item };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            //Simulate aging process
            
            app.UpdateQuality();

            Assert.True(item.Quality <= 50);
        }

        public static IEnumerable<object[]> QualityMaxTestData()
        {
            return new List<object[]>
            {
                new object[]{ new Item{Name = NormalItemName, Quality = 1, SellIn = -1}},
                new object[]{ new Item{Name = AgedBrieItemName, Quality = 49, SellIn = -1}},
                new object[]{ new Item{Name = BackstagePassesItemName, Quality = 49, SellIn = 12}},
                new object[]{ new Item{Name = BackstagePassesItemName, Quality = 49, SellIn = 7}},
                new object[]{ new Item{Name = BackstagePassesItemName, Quality = 49, SellIn = 3}},
                new object[]{ new Item{Name = BackstagePassesItemName, Quality = 49, SellIn = 1}},
            };
        }
        #endregion

        #region Sulfuras
        [Fact]
        public void GildedRose_Sulfuras_Never_Changes()
        {
            IList<Item> Items = new List<Item> { new Item{Name = SulfurasItemName, Quality = 80, SellIn = -1} };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            //Simulate aging process
            app.UpdateQuality();

            var item = Items[0];
            Assert.Equal(80, item.Quality);
            Assert.Equal(-1, item.SellIn);
        }
        #endregion

        #region Backstage passes
        [Fact]
        public void GildedRose_Backstage_Passes_Quality_Increases_By_1_If_More_Than_10_Days_Till_Concert()
        {
            var sellin = 15;
            var quality = 10;
            IList<Item> Items = new List<Item> { new Item { Name = BackstagePassesItemName, SellIn = sellin, Quality = quality } };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            app.UpdateQuality();

            var item = Items[0];
            Assert.Equal(quality + 1, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality + 2, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality + 3, item.Quality);
        }

        [Fact]
        public void GildedRose_Backstage_Passes_Quality_Increases_By_2_If_Less_Or_Equal_10_Days_Till_Concert()
        {
            var sellin = 10;
            var quality = 10;
            IList<Item> Items = new List<Item> { new Item { Name = BackstagePassesItemName, SellIn = sellin, Quality = quality } };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            app.UpdateQuality();

            var item = Items[0];
            Assert.Equal(quality + 2, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality + 4, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality + 6, item.Quality);
        }

        [Fact]
        public void GildedRose_Backstage_Passes_Quality_Increases_By_3_If_Less_Or_Equal_5_Days_Till_Concert()
        {
            var sellin = 5;
            var quality = 10;
            IList<Item> Items = new List<Item> { new Item { Name = BackstagePassesItemName, SellIn = sellin, Quality = quality } };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            app.UpdateQuality();

            var item = Items[0];
            Assert.Equal(quality + 3, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality + 6, item.Quality);

            app.UpdateQuality();
            Assert.Equal(quality + 9, item.Quality);
        }

        [Fact]
        public void GildedRose_Backstage_Passes_Quality_Drops_To_0_After_Concert()
        {
            var sellin = 0;
            var quality = 10;
            IList<Item> Items = new List<Item> { new Item { Name = BackstagePassesItemName, SellIn = sellin, Quality = quality } };
            GildedRose app = new GildedRose(Items, ItemAgingStrategyResolver);
            
            app.UpdateQuality();

            var item = Items[0];
            Assert.Equal(0, item.Quality);
        }

        #endregion

        #region Conjured Items

        [Fact]
        public void GildedRose_ConjuredItem_Should_Degrade_Twice_Faster()
        {
            var sellin = 1;
            var quality = 17;

            var items = new List<Item>{new Item { Name = ConjuredItemName, SellIn = sellin, Quality = quality }};
            var app = new GildedRose(items, ItemAgingStrategyResolver);
            app.UpdateQuality();

            var item = items[0];

            Assert.Equal(sellin - 1, item.SellIn);
            Assert.Equal(quality - 2, item.Quality);
        }

        [Fact]
        public void GildedRose_ConjuredItem_Should_Degrade_4x_Faster_After_Sellin()
        {
            var sellin = 0;
            var quality = 17;

            var items = new List<Item>{new Item { Name = ConjuredItemName, SellIn = sellin, Quality = quality }};
            var app = new GildedRose(items, ItemAgingStrategyResolver);

            app.UpdateQuality();

            var item = items[0];
            Assert.Equal(sellin - 1, item.SellIn);
            Assert.Equal(quality - 4, item.Quality);
        }
        #endregion
    }
}