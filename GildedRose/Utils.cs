namespace GildedRose
{
    public class Utils
    {
        /// <summary>
        /// Forced a value into a specific range.
        /// </summary>
        /// <param name="value">Value to work on</param>
        /// <param name="min">Minimum allowed value</param>
        /// <param name="max">Maximum allowed value</param>
        /// <returns>A value, forced into the range</returns>
        public static int Clamp(int value, int min, int max)
        {
            if(value > max)
            {
                value = max;
            }
            else if(value < min)
            {
                value = min;
            }
            return value;
        }
    }
}